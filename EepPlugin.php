<?php
namespace Craft;

class EepPlugin extends BasePlugin
{
    function getName()
    {
         return Craft::t('Ecom Engine Proxy');
    }

    function getVersion()
    {
        return '2.0.0';
    }

    function getDeveloper()
    {
        return 'SeaLink Travel Group';
    }

    function getDeveloperUrl()
    {
        return 'https://sealinktravelgroup.com.au';
    }

    public function hasCpSection()
    {
        return false;
    }

    protected function defineSettings()
    {
      return array(
        'enabled' => array(AttributeType::Bool, 'default' => true)
      );
    }

    public function getSettingsHtml()
    {
      return craft()->templates->render('eep/settings', array(
        'settings' => $this->getSettings(),
      ));
    }

    //news/(\d{4})/(\d{2})
    public function registerSiteRoutes() {

      $ecom_engine = array('action' => '/eep/request/mountPage');

      $ecom_engine_matches = [
        'cells(.*)',
        'ecom-engine-js/(.*)',
        'cart/checkout/summary(.*)'
      ];

      if ($this->settings->enabled == true) {
        $eep_enabled_matches = [
          'parties/login',
          '([a-z-]*-accommodation\/\d+.*)',
          'search/accommodations',
          'search/tickets',
          'search/grid',
          'transport(\/.*)',
          'transport',
          'tour(\/.*)',
          'cart(\/.*)',
          'secure-payment',
          'cart_activations',
          'cart_activations(\/.*)',
          'bookings/manage(\/.*)',
          'parties(\/.*)',
          'documents(\/.*)',
          'reset_password',
          'reset_password(\/.*)',
          'reservations/(.*)'
        ];

        $ecom_engine_matches = array_merge($ecom_engine_matches, $eep_enabled_matches);
      }

      $routes = array();
      foreach ($ecom_engine_matches as $match)
        $routes[$match] = $ecom_engine;
      return $routes;
    }

}
