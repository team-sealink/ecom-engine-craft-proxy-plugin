# IMPORTANT: This project is deprecated, please make changes directly to 1SL.
# Ecom Engine Craft Proxy

## 2.0.0

* [DC-1464] Toggle Ecom Engine routes for non-booking sites

## 1.6.0

* [TT-2553] Return the status code from ecom-engine

## 1.5.0

* [TT-2553] Add routes for Book Box and Booking Grid

## 1.4.0

* [ME-241] Add Reset Password route

## 1.3.0

* [ME-222] Rename route `secure-pay` to `secure-payment`

## 1.2.0

* Rename of Booking App to Ecom Engine

## 1.1.1

* [TT-605] Add search/tickets route

## 1.1.0

* Increased timeout
* Set all layouts to load from folder `bap-layouts`
* Added default templates

## 1.0.0

* Initial release
