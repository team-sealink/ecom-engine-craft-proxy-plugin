<?php
namespace Craft;

/**
 * Ecom Engine Proxy base controller
 */
class Eep_RequestController extends BaseController
{
  protected $allowAnonymous = true; // Allow anyone to access content

  // Direct access: http://craft.dev/actions/eep/request/mountpage
  public function actionMountPage() {
    $attributes = craft()->request->getPost();
    $response = craft()->eep->sendToEe($attributes);

    if (!is_array($response)) {
      $variables['html'] = $response;
      $this->renderTemplate('eep-layouts/_error', $variables);
      return;
    }

    list($http_status, $headers_array, $content_type, $type, $html) = $response;
    $variables['html'] = $html;
    $uri = craft()->request->getRequestUri();
    $variables['content_type'] = 'Content-Type: '.$content_type;
    http_response_code($http_status);

    if ($type == 'application/json' || $type == 'application/pdf' || $type == 'application/javascript' || substr($uri, 0, 6) == '/cells') {
      $this->renderTemplate('eep-layouts/_blank', $variables);
    } else {
      $this->renderTemplate('eep-layouts', $variables);
    }
  }

}
