# IMPORTANT: This project is deprecated, please make changes directly to 1SL.
# Ecom Engine Proxy for Craft CMS

Proxies responses through from the following paths
```
'parties/login',
'([a-z-]*-accommodation\/\d+.*)',
'search/accommodations',
'search/tickets',
'search/grid',
'transport(\/.*)',
'transport',
'tour(\/.*)',
'cells(.*)',
'cart(\/.*)',
'secure-payment',
'cart_activations',
'cart_activations(\/.*)',
'bookings/manage(\/.*)',
'parties(\/.*)',
'documents(\/.*)',
'ecom-engine-js/(.*)',
'reset_password',
'reset_password(\/.*)',
'reservations/(.*)'
```

Requests from 'application/json', 'application/pdf', 'application/javascript responses render into `'craft/templates/eep-layouts/_blank'`
else they render into `'craft/templates/eep-layouts/index'`

Error request render to 'craft/templates/eep-layouts/_error'

## Getting started ##

* Copy the sample-templates folder to into your templates
* Clone, copy or use composer to use in your project

#### Composer Use ####
Add the following to your `composer.json`

```
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://bitbucket.org/team-sealink/ecom-engine-craft-proxy-plugin.git"
    },
  ]
  "require": {
    "sealink/ecom-engine-craft-proxy": "2.0.0"
  }
}
```
