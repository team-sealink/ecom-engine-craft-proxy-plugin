<?php
namespace Craft;
  // https://kis-next-ecom-engine.quicktravel.com.au/
/**
 * Ecom Engine Proxy service
 */

class EepService extends BaseApplicationComponent
{
  private $http_status, $header_size, $error;

  public function sendToEe($attributes = null) {
    $h = $this->makeRequest($attributes);

    if ($this->error)
      return $this->errorHandler();

    $headers  = substr($h, 0, $this->header_size);
    $response = substr($h, $this->header_size);

    $headers_array = $this->getHeadersAsArraryFromString($headers);
    if ($this->http_status == '302') {
      $this->handleRedirect($headers_array['Location']);
      return;
    }

    if (isset($headers_array['Set-Cookie']))
      $_SESSION['proxied_cookies'] = $headers_array["Set-Cookie"];

    if (isset($headers_array['Content-Type'])) {
      $content_type = $headers_array['Content-Type'];
      list($type, $stuff) = explode('; ', $content_type.'; ', 2);
    } else {
      $type = $content_type = 'text/html';
    }
    return [$this->http_status, $headers_array, $content_type, $type, (string)$response];
  }

  private function errorHandler() {
    if (!craft()->config->get('ecomEngineURL')) {
      return 'Error: Have you set the Ecom Engine URL in general.php or your docker-compose.yml? The Actual Error:' . $this->error;
    } else {
      return 'Could not communicate with booking system: ' . craft()->config->get('ecomEngineURL') . " : " . $this->error;
    }
  }

  private function handleRedirect($header_location) {
    $location = parse_url($header_location);
    $query_string = '';
    if (isset($location['query']))
      $query_string .= '?' . $location['query'];
    header('Location: '.$location['path'].$query_string);
  }

  private function makeRequest($attributes) {
    $ch = curl_init();
    $headers = $this->splitHeaders(['X-CSRF-Token', 'Accept']);
    if (!isset($headers['Accept']) && isset($_SERVER['HTTP_ACCEPT']))
      $headers['Accept'] = $_SERVER['HTTP_ACCEPT'];
    $options = array(
      CURLOPT_HEADER         => true,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_VERBOSE        => true,
      CURLOPT_TIMEOUT        => 60,
      CURLOPT_URL            => $this->requestUrl(),
      CURLOPT_HTTPHEADER     => $headers,
    );

    if (isset($_SERVER['HTTP_REFERER']))
      $options[CURLOPT_REFERER] = $_SERVER['HTTP_REFERER'];
    if (isset($_SESSION['proxied_cookies']))
      $options[CURLOPT_COOKIE]  = $_SESSION['proxied_cookies'];

    switch ($_SERVER['REQUEST_METHOD']) {
      case 'POST':
        $options[CURLOPT_POSTFIELDS] = http_build_query($attributes);
        break;
      case 'PUT':
        $body = file_get_contents('php://input');
        $options[CURLOPT_CUSTOMREQUEST] = 'PUT';
        $options[CURLOPT_POSTFIELDS] = $body;
        break;
      case 'DELETE':
        $body = file_get_contents('php://input');
        $options[CURLOPT_CUSTOMREQUEST] = 'DELETE';
        $options[CURLOPT_POSTFIELDS] = $body;
        break;
    }

    curl_setopt_array($ch, $options);
    $h = curl_exec($ch);
    $this->http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $this->header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    if (!$h)
      $this->error = curl_error($ch);
    curl_close($ch);
    return $h;
  }

  private function requestUrl() {
    $host_param = '_proxied_host='.$_SERVER['HTTP_HOST'];
    $parts = explode('?', $_SERVER['REQUEST_URI'], 2);
    $path = $parts[0];
    if (isset($parts[1]))
      $params = '?' . $parts[1] . '&' . $host_param;
    else
      $params = '?' . $host_param;
    return $this->getBaseURL() . $path . $params;
  }

  public function getBaseURL() {
    return craft()->config->get('ecomEngineURL');
  }

  private function getHeadersAsArraryFromString($header_text) {
    // Sometimes multiple headers appear, e.g. HTTP 100 Continue
    // The last one is the only relevant one
    $raw_headers = explode("\r\n\r\n", rtrim($header_text)); // remove trailing \r\n
    $header = end($raw_headers);

    $headers = array();
    foreach (explode("\r\n", $header) as $i => $line)
      if (substr($line, 0, 5) == 'HTTP/') {
        $headers['http_code'] = $line;
      } else {
        list ($key, $value) = explode(': ', $line);
        $headers[$key] = $value;
      }
    return $headers;
  }

  private function splitHeaders($keys) {
    $arr = array();
    foreach ($keys as $index) {
      if (isset($_SERVER[$index]))
        $arr[$index] = $_SERVER[$index];
    }
    return $arr;
  }

}
