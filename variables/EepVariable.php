<?php
namespace Craft;

class EepVariable
{
    /**
     * Return HTML Response.
     *
     * @example {{ craft.eep.getEeHTML }}
     * @return string
     */
    public function getEeHTML()
    {
        return craft()->eep->getEeHTML();
    }

    public function ecomEngineURL()
    {
        return craft()->eep->getBaseURL();
    }

    public function getResponseCode()
    {
        return craft()->eep->getResponseCode();
    }

    public function getURLComponents()
    {
        $url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        return $_SERVER['REQUEST_URI'];
    }
};
